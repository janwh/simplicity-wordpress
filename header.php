<?php
/**
 * @package WordPress
 * @subpackage Simplicity
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

	<style type="text/css" media="screen">
		@import url( <?php bloginfo('stylesheet_url'); ?> );
	</style>
	
	
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/images/icon.png"/>
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/icon-2x.png"/>
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/icon72.png"/>
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/images/icon72-2x.png"/>


	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); wp_head(); ?>	

</head>
<body <?php body_class(); ?>>

<div id="left">
	<div id="menubar">
		<ul>
			<li class="widget">
				<a href="<?php bloginfo('url'); ?>" class="nounderline">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?> Logo" class="aligncenter logo" />
				</a>
			</li>
			<?php if ( !function_exists('dynamic_sidebar') ||
			           !dynamic_sidebar('Menu bar left') ) : ?>
			<?php endif; ?>
		</ul>
	</div>
</div>

<div id="rap">

<div id="header">
	<a href="<?php bloginfo('url'); ?>/" class="blogtitle">
		<?php bloginfo('name'); ?>
	</a>
	
	<a href="<?php bloginfo('url'); ?>/" class="bloglogo">
		<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?> Logo" class="aligncenter logo" />
	</a>
</div>

<div id="content">

<!-- end header -->