<?php
/**
 * @package WordPress
 * @subpackage Simplicity
 */

automatic_feed_links();

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Menu bar left',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '',
		'after_title' => '',
	));
	register_sidebar(array(
		'name' => 'Footer bar small',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '',
		'after_title' => '',
	));
		
function remove_more_jump_link($link) { 
$offset = strpos($link, '#more-');
if ($offset) {
$end = strpos($link, '"',$offset);
}
if ($end) {
$link = substr_replace($link, '', $offset, $end-$offset);
}
return $link;
}
add_filter('the_content_more_link', 'remove_more_jump_link');
add_theme_support( 'post-thumbnails' );

?>
