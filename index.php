<?php
/**
 * @package WordPress
 * @subpackage Simplicity
 */
get_header();
?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
	
	<h1 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
	</h1>
	
	<p class="storydate"><?php the_time('j. F Y') ?> <?php edit_post_link(__('&nbsp;&#8984; Bearbeiten&nbsp;')); ?></p>

	<?php if (has_post_thumbnail()) { ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
    <?php } ?><div class="storycontent">
		<?php the_content('<p class="moretext">Weiterlesen &raquo;</p>'); ?>
			</div>
		<hr>

	
</div>


<!--<?php comments_template(); // Get wp-comments.php template ?>-->

<?php endwhile; else: ?>
<div id="pages">
	<h1>Uuups!</h1>
	<div id="search_page">
		<p>
			<?php _e('Entschuldige bitte! Keiner meiner Artikel enthält dein Suchwort.'); ?>
		</p>
	</div>
	
	<p class="moretext">
		<a href="<?php bloginfo('url'); ?>/">Zur Startseite zur&uuml;ckkehren &raquo;</a>
	</p>
	
	<hr>
</div>
<?php endif; ?>


<div id="nav">
	<div class="alignleft"><?php previous_posts_link('&laquo; Vorherige Seite') ?></div>
	<div class="alignright"><?php next_posts_link('N&auml;chste Seite &raquo;','') ?></div>
</div>
<hr>




<?php get_footer(); ?>
